package com.recruit.scraper;

import org.openqa.selenium.WebDriver;

import com.rfx.exceptions.FirefoxDriverException;

public interface Scraper {
	void configureDriverForUrl(final String url) throws FirefoxDriverException;

	void login();

	void SearchButton(WebDriver driver);

	void SearchByKeyword(WebDriver driver);
}
