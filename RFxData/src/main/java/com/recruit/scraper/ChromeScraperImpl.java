package com.recruit.scraper;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.recruit.webdriver.ChromeWebDriver;
import com.recruit.webdriver.Driver;
import com.rfx.exceptions.ChromeDriverException;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class ChromeScraperImpl implements Scraper {

	@Autowired
	Environment env;

	private static final Logger log = Logger.getLogger(ChromeScraperImpl.class.getName());
	private static final int MAX_WAIT_SECONDS = 10;
	private Driver chrome;
	private WebDriver driver;
	WebDriverWait wait;

	{
		chrome = new ChromeWebDriver();
	}

	@Override
	public void configureDriverForUrl(final String url) throws ChromeDriverException {
		driver = chrome.configureWebDriver(url);
		wait = new WebDriverWait(driver, MAX_WAIT_SECONDS);
		login();
		SearchButton(driver);

	}

	@Override
	public void login() {
		// String userName = env.getProperty("bidman.username");
		// String password = env.getProperty("bidmain.password");
		try {

			driver.findElement(By.name("userid")).sendKeys("solomonpaul2");
			driver.findElement(By.name("pwd")).sendKeys("GeoSpoc123");
			driver.findElement(By.xpath("//input[starts-with(@alt,'Government Bid, Contract, Contracts, Bids')]"))
					.click();

			System.out.println("Successfully Login");
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	@Override
	public void SearchButton(WebDriver driver) {
		driver.findElement(By.xpath("//a[starts-with(@href, \"/government_bids/index.htm\")]")).click();
		driver.findElement(By.xpath("//a[starts-with(@href, \"/government_bids/view.htm\")]")).click();
		String result = driver.findElement(By.xpath(".//*[@class='content_padding']")).getText();

//		List<WebElement> col = driver
//				.findElements(By.xpath(".//*[@class=\"content_padding\"]/table[5]//tbody[1]//tr[1]//td[2])']"));

		WebElement github = driver.findElement(By.xpath(".//*[@class='content_padding']"));

		// get the value of the "title" attribute of the github icon
		String actualTooltip = github.getAttribute("title");
		System.out.println(actualTooltip);
		int size = driver.findElements(By.tagName("td")).size();
		System.out.println(result);
	}
//new try
	@Override
	public void SearchByKeyword(WebDriver driver) {
		List<String> keywords = Arrays.asList(" GIS", "Application Development", "Artificial intelligence",
				"Blockchain", "chatbot", "contingent staff", "custom Development", "Custom Programming",
				"custom software", "data analytics", "Data Science", "Data Scientist", "Drone data analysis",
				"Drone data processing", "Esri", "Gis Development", "Gis portal", "Image processing",
				"Information Technology Consulting", "Information Technology professional",
				"Information Technology staff", "IT consulting", "IT professional", "machine learning", "peoplesoft",
				"professional services", "recruiting", "Remote sensing", "Staff Augmentation", "Staffing Support",
				"system implementation", "Temporary staff", "tree counting");

		for (String data : keywords) {
			saveInDb();
		}

	}

	private void saveInDb() {

	}

}
