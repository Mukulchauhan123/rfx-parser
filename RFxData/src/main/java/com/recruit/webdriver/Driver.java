package com.recruit.webdriver;

import org.openqa.selenium.WebDriver;

import com.rfx.exceptions.FirefoxDriverException;

public interface Driver {
	WebDriver configureWebDriver(final String url) throws FirefoxDriverException;
}
