package com.recruit.webdriver;

import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.rfx.exceptions.ChromeDriverException;

public class ChromeWebDriver implements Driver {
	private static final Logger LOGGER = Logger.getLogger(ChromeWebDriver.class.getName());

	public WebDriver configureWebDriver(final String url) throws ChromeDriverException {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		try {
			driver.manage().window().maximize();
			driver.get(url);
			
			
		} catch (Exception e) {
			if (driver != null) {
				driver.close();
				driver.quit();
			}
			throw e;
		}

		return driver;
	}

}
