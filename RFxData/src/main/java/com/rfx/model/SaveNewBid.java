package com.rfx.model;

import lombok.Data;

@Data
public class SaveNewBid {

	private String rFxTitle;
	private String rFxType;
	private String stateProvince;
	private String rFxNumber;
	private String rFxClientAgencySourceURL;
	private String rFxPublishDate;
	private String rFxDueDateandTime;
	private String buyerName;
	private String buyerPosition;
	private String buyerPhone;
	private String scope;

}
