package com.rfx.exceptions;

public class ChromeDriverException extends BaseException {
	private static final long serialVersionUID = -2297995493806740858L;


	public ChromeDriverException(final String message) {
        super(message);
    }


    public ChromeDriverException(final String message, final Throwable cause) {
        super(message, cause);
    }
}