package com.rfx;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.recruit.scraper.ChromeScraperImpl;

@SpringBootApplication
public class RFxDataApplication {
	private final static String BIDMAIN_PAGE_URL = "https://www.bidmain.com/";
	private ChromeScraperImpl scraper = null;

	public static void main(String[] args) {
		RFxDataApplication app = new RFxDataApplication();
		app.configure();
	}

	private void configure() {
		scraper = new ChromeScraperImpl();
		scraper.configureDriverForUrl(BIDMAIN_PAGE_URL);

	}
}